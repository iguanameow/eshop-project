package com.eShop.backend.mapper;

import com.eShop.backend.dto.ItemDto;
import com.eShop.backend.model.ItemEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemMapper {

  public List<ItemDto> toDto(List<ItemEntity> entities) {
    return entities.stream().map(this::toDto).collect(java.util.stream.Collectors.toList());
  }

  public ItemDto toDto(ItemEntity itemEntity) {
    var dto = new ItemDto();

    if (itemEntity.getId() != null) {
      dto.setId(itemEntity.getId());
    }
    dto.setName(itemEntity.getName());
    dto.setPrice(itemEntity.getPrice());
    dto.setCategory(itemEntity.getCategory());
    dto.setDescription(itemEntity.getDescription());
    dto.setStock(itemEntity.getStock());
    dto.setImage(itemEntity.getImage());
    return dto;
  }

  public List<ItemEntity> toEntity(List<ItemDto> dtos) {
    return dtos.stream().map(this::toEntity).collect(java.util.stream.Collectors.toList());
  }

  public ItemEntity toEntity(ItemDto itemDto) {
    var entity = new ItemEntity();

    if (itemDto.getId() != null) {
      entity.setId(itemDto.getId());
    }
    entity.setName(itemDto.getName());
    entity.setPrice(itemDto.getPrice());
    entity.setCategory(itemDto.getCategory());
    entity.setDescription(itemDto.getDescription());
    entity.setStock(itemDto.getStock());
    entity.setImage(itemDto.getImage());

    return entity;
  }
}
