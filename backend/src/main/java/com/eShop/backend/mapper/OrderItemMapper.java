package com.eShop.backend.mapper;

import com.eShop.backend.dto.OrderItemDto;
import com.eShop.backend.model.OrderItemEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemMapper {

  public List<OrderItemDto> toDto(List<OrderItemEntity> entities) {
    return entities.stream().map(this::toDto).collect(java.util.stream.Collectors.toList());
  }

  public OrderItemDto toDto(OrderItemEntity OrderItemDto) {
    var dto = new OrderItemDto();
    dto.setItemId(OrderItemDto.getItem().getId());
    dto.setQuantity(OrderItemDto.getQuantity());
    return dto;
  }
}
