package com.eShop.backend.service;

import com.eShop.backend.model.UserEntity;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;
import java.util.UUID;

public interface IUserService {
  /**
   * @return a list of all users present in the database
   */
  Iterable<UserEntity> getAllUsers();

  /**
   * Encodes the password provided by the user in the frontend
   * and creates a new entry in the database to save it
   *
   * @param user UserEntity containing all the information required
   */
  void saveUser(UserEntity user);

  /**
   * Removes the user whose UUID was provided from the database
   *
   * @param id UUID to identify the user to delete
   */
  void deleteUser(UUID id);

  /**
   * Retrieves from the database a given user whose username was provided
   * If no user with such username was found, throws an exception "User not found"
   *
   * @param username Email to identify the user
   * @return User information contained in a UserEntity that implements the UserDetails interface
   */
  UserDetails loadUserByUsername(String username);

  /**
   * Retrieves user from the database thanks to their UUID
   *
   * @param id UUID to identify the user
   * @return Optional container that can be either a UserEntity if the user was successfully identified
   * or empty if no user corresponds to the UUID provided
   */
  Optional<UserEntity> getUserById(UUID id);
}
