package com.eShop.backend.service.impl;

import com.eShop.backend.dto.*;
import com.eShop.backend.model.*;
import com.eShop.backend.repository.ItemRepository;
import com.eShop.backend.repository.OrderItemRepository;
import com.eShop.backend.repository.OrderRepository;
import com.eShop.backend.repository.UserRepository;
import com.eShop.backend.service.IOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class OrderService implements IOrderService {
  private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

  private final OrderRepository orderRepository;
  private final ItemRepository itemRepository;
  private final OrderItemRepository orderItemRepository;
  private final UserRepository userRepository;

  @Autowired
  public OrderService(OrderRepository orderRepository, ItemRepository itemRepository,
                      OrderItemRepository orderItemRepository, UserRepository userRepository) {
    this.orderRepository = orderRepository;
    this.itemRepository = itemRepository;
    this.orderItemRepository = orderItemRepository;
    this.userRepository = userRepository;
  }

  public Optional<OrderEntity> getOrderById(UUID id) {
    LOGGER.info("Getting order with id: {}", id);
    Optional<OrderEntity> order = orderRepository.findById(id);
    if (order.isEmpty()) {
      LOGGER.info("Order with id: {} not found", id);
      return Optional.empty();
    }
    LOGGER.info("Order with id: {} found", id);
    return order;
  }

  public boolean updateOrder(OrderEntity order) {
    try {
      OrderEntity dbOrder = orderRepository.getById(order.getId());

      dbOrder.setCustomerFirstName(order.getCustomerFirstName());
      dbOrder.setCustomerLastName(order.getCustomerLastName());
      dbOrder.setCustomerAddress(order.getCustomerAddress());
      dbOrder.setCustomerPhone(order.getCustomerPhone());

      LOGGER.info("Updating order with id: {}", order.getId());
      orderRepository.save(dbOrder);

    } catch (IllegalArgumentException | EntityNotFoundException e) {
      LOGGER.warn("Failed to update order: {}", order.getId(), e);
      return false;
    }
    return true;
  }

  public Optional<OrderEntity> getCartForUser(UUID customerId) {
    return orderRepository.findAll().stream()
      .filter(orderEntity -> orderEntity.getCustomer().getId().equals(customerId)
        && orderEntity.getOrderStatus().equals(OrderStatusEnum.CART))
      .findFirst();
  }

  public UUID getCustomerOrderIdOrCreate(UUID customerId) {
    LOGGER.info("Getting customer ({}) order or creating new one", customerId);
    Optional<OrderEntity> orderEntity = getCartForUser(customerId);

    if (orderEntity.isEmpty()) {
      LOGGER.info("Customer ({}) order not found, creating new one", customerId);
      UserEntity user = userRepository.getById(customerId);
      var newCartOrder = new OrderEntity();
      newCartOrder.setCustomer(user);
      newCartOrder.setOrderStatus(OrderStatusEnum.CART);
      orderEntity = Optional.of(newCartOrder);
      orderRepository.save(newCartOrder);
      System.out.println(newCartOrder.getId());
    }

    return orderEntity.get().getId();
  }

  public AddItemToCartResponse addItemToCart(AddItemToCartRequest addItemToCartRequest) {
    LOGGER.info("Adding item ({}) to cart", addItemToCartRequest.getItemId());
    AddItemToCartResponse response = new AddItemToCartResponse();
    if (itemRepository.getById(addItemToCartRequest.getItemId()).getDeleted()) {
      response.setMessage("Item was removed");
      response.setRequestStatus(false);
      return response;
    }

    if (addItemToCartRequest.getOrderId() == null) {
      if (addItemToCartRequest.getCustomerId() != null) {
        addItemToCartRequest
          .setOrderId(getCustomerOrderIdOrCreate(addItemToCartRequest.getCustomerId()));
      } else {
        LOGGER.info("Customer id is null, cannot infer cart id");
        response.setMessage("Customer id is null, cannot infer cart id");
        response.setRequestStatus(false);
        return response;
      }
    }

    if (orderRepository.existsById(addItemToCartRequest.getOrderId())) {
      OrderEntity orderEntity = orderRepository.getById(addItemToCartRequest.getOrderId());
      ItemEntity item = itemRepository.getById(addItemToCartRequest.getItemId());
      int quantity = addItemToCartRequest.getQuantity();

      if (orderEntity.getOrderStatus().equals(OrderStatusEnum.CART) && item.getStock() >= quantity) {

        boolean itemExist = false;

        for (int i = 0; i < orderEntity.getItems().size(); i++) {

          if (orderEntity.getItems().get(i).getItem().getId() == addItemToCartRequest.getItemId()) { // update quantity

            itemExist = true;
            int currentQuantity = orderEntity.getItems().get(i).getQuantity();
            orderEntity.getItems().get(i).setQuantity(currentQuantity + quantity);
            orderRepository.save(orderEntity);

            item.setStock(item.getStock() - quantity);
            itemRepository.save(item);

            LOGGER.info("Item ({}) added to cart ({})", addItemToCartRequest.getItemId(),
              addItemToCartRequest.getOrderId());
            response.setRequestStatus(true);
            response.setMessage("Item added to cart: quantity updated");

            break;
          }

        }

        if (!itemExist) { // add new order
          OrderItemEntity orderItemEntity = new OrderItemEntity();
          orderItemEntity.setItem(item);
          orderItemEntity.setQuantity(addItemToCartRequest.getQuantity());
          orderItemEntity.setOrder(orderEntity);
          orderItemRepository.save(orderItemEntity);

          item.setStock(item.getStock() - quantity);
          itemRepository.save(item);

          List<OrderItemEntity> items = orderEntity.getItems();
          items.add(orderItemEntity);
          orderEntity.setItems(items);
          orderRepository.save(orderEntity);
          LOGGER.info("Item ({}) added to cart ({})", addItemToCartRequest.getItemId(),
            addItemToCartRequest.getOrderId());
          response.setRequestStatus(true);
          response.setMessage("Item added to cart");
        }
      } else {
        LOGGER.info("Item ({}) not added to cart ({}) - out of stock",
          addItemToCartRequest.getItemId(), addItemToCartRequest.getOrderId());
        response.setRequestStatus(false);
        response.setMessage("Out of stock!");
      }
    } else {
      LOGGER.info("Cart ({}) not found", addItemToCartRequest.getOrderId());
      response.setRequestStatus(false);
      response.setMessage("Order doesn't exist!");
    }

    return response;
  }

  public void removeItemFromCart(UUID orderItemID) {

    OrderItemEntity orderItemEntity = orderItemRepository.getById(orderItemID);
    ItemEntity item = itemRepository.getById(orderItemEntity.getItem().getId());
    int cartQuantity = orderItemEntity.getQuantity();

    OrderEntity order = orderItemEntity.getOrder();
    List<OrderItemEntity> cartItems = order.getItems();

    // Do not attempt to remove the item if it was already marked as deleted
    if (itemRepository.getById(orderItemEntity.getItem().getId()).getDeleted()
      && !orderItemRepository.findById(orderItemID).isPresent()) {
      return;
    }


    for (int i = 0; i < cartItems.size(); i++) {
      if (cartItems.get(i).getId().equals(orderItemID)) {

        cartItems.remove(i);
        orderItemRepository.deleteById(orderItemID);
        order.setItems(cartItems);

        //Save order
        orderRepository.save(order);
        LOGGER.info("Item removed from cart");

        //Update Quantity

        item.setStock(item.getStock() + cartQuantity);
        itemRepository.save(item);

        break;
      }
    }

  }

  public void createOrder(OrderEntity orderEntity) {
    orderRepository.save(orderEntity);
  }

  public List<OrderEntity> getOrders() {
    return orderRepository.findAll();
  }

  public OrderEntity updateOrderStatus(UUID orderId, OrderStatusEnum orderStatus) {
    OrderEntity order = orderRepository.findById(orderId).get();
    order.setOrderStatus(orderStatus);
    return orderRepository.save(order);
  }

  public OrderItemsCountResponse getOrderItemsCount(OrderItemsCountRequest itemsCount) {
    if (itemsCount.getOrderId() != null) {
      OrderEntity orderEntity = orderRepository.getById(itemsCount.getOrderId());
      LOGGER.info("Getting order items count for order ({})", itemsCount.getOrderId());
      return new OrderItemsCountResponse(true, orderEntity.getItems().size());
    } else if (itemsCount.getUserId() != null) {
      Optional<OrderEntity> orderEntity = this.getCartForUser(itemsCount.getUserId());
      if (orderEntity.isPresent()) {
        LOGGER.debug("Found cart for user ({})", itemsCount.getUserId());
        return new OrderItemsCountResponse(true, orderEntity.get().getItems().size());
      } else {
        LOGGER.debug("No cart found for user ({})", itemsCount.getUserId());
        return new OrderItemsCountResponse(false, 0);
      }
    }
    return new OrderItemsCountResponse(true, 0);
  }

  public void removeOrderItemFromInCartOrders(UUID itemId) {
    if (orderRepository.findAllInCart().isPresent()) {
      List<OrderEntity> inCartOrders = orderRepository.findAllInCart().get();

      for (OrderEntity inCartOrder : inCartOrders) {
        List<OrderItemEntity> orderItems = inCartOrder.getItems();

        for (OrderItemEntity orderItem : orderItems) {
          if (orderItem.getItem().getId().equals(itemId)) {
            RemoveCartItemRequest removeCartItemRequest = new RemoveCartItemRequest();
            removeCartItemRequest.setItemId(itemId);
            removeCartItemRequest.setOrderItemId(orderItem.getId());
            removeCartItemRequest.setOrderId(inCartOrder.getId());

            removeItemFromCart(removeCartItemRequest.getOrderItemId());
          }
        }
      }
    }
  }

  public boolean updateOrderStatusToPaid(UUID orderId) {
    LOGGER.info("Updating order status to paid for order ({})", orderId);
    OrderEntity orderEntity = orderRepository.getById(orderId);
    orderEntity.setOrderStatus(OrderStatusEnum.ORDERED);
    orderRepository.save(orderEntity);
    return true;
  }
}
