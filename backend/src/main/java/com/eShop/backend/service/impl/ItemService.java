package com.eShop.backend.service.impl;

import com.eShop.backend.dto.AdminItemResponse;
import com.eShop.backend.dto.ItemDto;
import com.eShop.backend.mapper.ItemMapper;
import com.eShop.backend.model.ItemEntity;
import com.eShop.backend.repository.ItemRepository;
import com.eShop.backend.service.IItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ItemService implements IItemService {
  private final ItemRepository itemRepository;
  private final OrderService orderService;
  private final ItemMapper itemMapper;

  public ItemService(ItemRepository itemRepository, OrderService orderService,
                     ItemMapper itemMapper) {
    this.itemRepository = itemRepository;
    this.orderService = orderService;
    this.itemMapper = itemMapper;
  }

  public List<ItemDto> getItems() {
    return itemMapper.toDto(itemRepository.findAll());
  }

  public ItemDto getItem(UUID itemId) {
    return itemMapper.toDto(itemRepository.findById(itemId).get());
  }

  public ResponseEntity<AdminItemResponse> createItem(ItemDto item) {
    AdminItemResponse response = new AdminItemResponse();
    itemRepository.save(itemMapper.toEntity(item));

    response.setStatusCode(200);
    response.setMessage("Item added successfully!");

    return ResponseEntity.ok(response);
  }

  public ResponseEntity<AdminItemResponse> updateItem(UUID itemId, ItemDto item) {
    ItemEntity it = itemRepository.findById(itemId).get();
    AdminItemResponse response = new AdminItemResponse();

    ItemEntity itemEnt = itemMapper.toEntity(item);

    it.setName(itemEnt.getName());
    it.setPrice(itemEnt.getPrice());
    it.setCategory(itemEnt.getCategory());
    it.setDescription(itemEnt.getDescription());
    it.setImage(itemEnt.getImage());

    if (itemEnt.getStock() >= 0) {
      it.setStock(itemEnt.getStock());
      itemRepository.save(it);

      response.setStatusCode(200);
      response.setMessage("Item stock updated successfully!");
      return ResponseEntity.ok(response);
    }

    response.setStatusCode(400);
    response.setMessage("Item stock update failed!");
    return ResponseEntity.badRequest().body(response);
  }

  public ResponseEntity<AdminItemResponse> deleteItem(UUID itemId) {
    itemRepository.deleteById(itemId);
    orderService.removeOrderItemFromInCartOrders(itemId);
    AdminItemResponse response = new AdminItemResponse();

    response.setStatusCode(200);
    response.setMessage("Item deleted successfully!");
    return ResponseEntity.ok(response);
  }
}
