package com.eShop.backend.service;

import com.eShop.backend.dto.AddItemToCartRequest;
import com.eShop.backend.dto.AddItemToCartResponse;
import com.eShop.backend.dto.OrderItemsCountRequest;
import com.eShop.backend.dto.OrderItemsCountResponse;
import com.eShop.backend.model.OrderEntity;
import com.eShop.backend.model.OrderStatusEnum;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IOrderService {

  /**
   * Gets an order entity by an ID, this method looks for an order entity with the passed ID in the database and
   * return it if found.
   *
   * @param id ID for the order
   * @return returns the order with the passed id if found or empty if not found
   */
  Optional<OrderEntity> getOrderById(UUID id);

  /**
   * Tries to update the orders' customer info and return true if succeeded, false if failed
   *
   * @param order ID for the order
   * @return a boolean value true if order updated and false if updating failed
   */
  boolean updateOrder(OrderEntity order);

  /**
   * Tries to find order for user and return it. If order is not found, the return values will be
   * empty.
   *
   * @param customerId id of user
   * @return order for user or an empty optional object
   */
  Optional<OrderEntity> getCartForUser(UUID customerId);

  /**
   * Tries to find order for the customer with customerId or create a new one if no order exist and return the order ID
   *
   * @param customerId id of user
   * @return the id of th order
   */
  UUID getCustomerOrderIdOrCreate(UUID customerId);

  /**
   * This method add an item to the cart, first it checks if the item was removed by admin or not, then it looks for the
   * item if its in stock and if the order is in CART status before adding the item, and lastly search for the item if
   * already exist in cart then update the quantity and if not add as a new item. After adding the item updates the stock
   *
   * @param addItemToCartRequest dto request that has item id, order id and quantity
   * @return a response message and boolean value that tells the result of the calling the method
   */
  AddItemToCartResponse addItemToCart(AddItemToCartRequest addItemToCartRequest);

  /**
   * Removes the item from the cart by the id of the orderItem, after removing the item it updates the stock
   *
   * @param orderItemID id of orderItem
   */
  void removeItemFromCart(UUID orderItemID);

  /**
   * Creates an order entity and store it in the database
   *
   * @param orderEntity
   */
  void createOrder(OrderEntity orderEntity);

  /**
   * Gets all existed orders
   *
   * @return a list of all existing orders
   */
  List<OrderEntity> getOrders();

  /**
   * Updates the order status of the order with the orderId to the passed status orderStatus
   *
   * @param orderId     ID for the order
   * @param orderStatus Enum status that can be CART, ORDERED or SHIPPED
   * @return
   */
  OrderEntity updateOrderStatus(UUID orderId, OrderStatusEnum orderStatus);

  /**
   * Counts the number of items exist in the cart
   *
   * @param itemsCount dto request that has used id and order id
   * @return a boolean requestStatus and integer item count
   */
  OrderItemsCountResponse getOrderItemsCount(OrderItemsCountRequest itemsCount);

  /**
   * @param itemId
   */
  void removeOrderItemFromInCartOrders(UUID itemId);


  /**
   * Updates the order status to ORDERED (Paid)
   *
   * @param orderId ID for the order
   * @return boolean value true as the status is updated
   */
  boolean updateOrderStatusToPaid(UUID orderId);


}
