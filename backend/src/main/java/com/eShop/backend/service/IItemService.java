package com.eShop.backend.service;

import com.eShop.backend.dto.AdminItemResponse;
import com.eShop.backend.dto.ItemDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface IItemService {
  /**
   * @return a list of all the items present in the database
   */
  List<ItemDto> getItems();

  /**
   * Retrieves a specific item from the database thanks to its ID.
   *
   * @param itemId UUID to identify the given item
   * @return ItemDto object that contains the item information
   */
  ItemDto getItem(UUID itemId);

  /**
   * Creates a new entry in the database to add a new item.
   *
   * @param item ItemDto object containing the item information
   * @return AdminItemResponse object containing a status to indicate if the creation was successful or not
   * and a message for further details
   */
  ResponseEntity<AdminItemResponse> createItem(ItemDto item);

  /**
   * Updates the information of a specific item identified by its ID
   * If the update fails, indicates an error.
   *
   * @param itemId UUID to identify the database item to be updated
   * @param item   ItemDto object containing the updated information
   * @return AdminItemResponse object containing a status to indicate if the update was successful or not
   * and a message for further details
   */
  ResponseEntity<AdminItemResponse> updateItem(UUID itemId, ItemDto item);

  /**
   * Deletes a specific item, identified by its ID, from the database
   * and from all the existing orders containing the OrderItem linked to it.
   *
   * @param itemId UUID to identify the item
   * @return AdminItemResponse object containing a status to indicate if the deletion was successful or not
   * and a message for further details
   */
  ResponseEntity<AdminItemResponse> deleteItem(UUID itemId);

}
