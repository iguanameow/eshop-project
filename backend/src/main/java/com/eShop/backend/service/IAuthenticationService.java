package com.eShop.backend.service;

import com.eShop.backend.dto.SignInRequest;
import com.eShop.backend.dto.SignInResponse;

public interface IAuthenticationService {
  /**
   * Checks if the username and password provided by the user that tries to log in match an existing user.
   * If so, the server will issue an electronic key (the authentication token) that the user will use
   * in every request they make to prove their identity. If the username is not found
   * or the password does not match the one stored, the SignInResponse will say so.
   *
   * @param request SignInRequest object containing the user email and password
   * @return SignInResponse object containing "success" and "message" fields that indicate
   * if the log in was successfull or not, and adequate user information such as the authentication token
   * if the authentication is successfull.
   */
  SignInResponse signIn(SignInRequest request);
}
