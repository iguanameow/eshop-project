package com.eShop.backend.service.impl;

import com.eShop.backend.dto.UserDetailsDto;
import com.eShop.backend.model.UserEntity;
import com.eShop.backend.repository.UserRepository;
import com.eShop.backend.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService, IUserService {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
    this.passwordEncoder = new BCryptPasswordEncoder();
  }


  public Iterable<UserEntity> getAllUsers() {
    return userRepository.findAll();
  }

  public void saveUser(UserEntity user) {
    String encodedPassword = this.passwordEncoder.encode(user.getPassword());
    user.setPassword(encodedPassword);
    userRepository.save(user);
  }

  public void deleteUser(UUID id) {
    userRepository.deleteById(id);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    LOGGER.info("Loading user by username: {}", username);
    Optional<UserEntity> user = userRepository.findByEmail(username);
    if (user.isEmpty()) {
      LOGGER.info("User for username {} not found", username);
      throw new UsernameNotFoundException("User not found");
    }
    LOGGER.info("User for username {} found", username);
    return new UserDetailsDto(user.get());
  }

  public Optional<UserEntity> getUserById(UUID id) {
    LOGGER.info("Loading user by id: {}", id);
    return userRepository.findById(id);
  }
}
