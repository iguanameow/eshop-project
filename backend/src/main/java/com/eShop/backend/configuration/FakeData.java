package com.eShop.backend.configuration;

import com.eShop.backend.dto.AddItemToCartRequest;
import com.eShop.backend.model.*;
import com.eShop.backend.repository.ItemRepository;
import com.eShop.backend.repository.OrderItemRepository;
import com.eShop.backend.repository.OrderRepository;
import com.eShop.backend.repository.UserRepository;
import com.eShop.backend.service.impl.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.*;

/**
 * Seeds random data to user_entity & item_entity in eshop_db
 * but unsure what more needs to be done or how to connect to frontend?
 */

@Configuration
// @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
public class FakeData {
  private static final Logger logger = LoggerFactory.getLogger(FakeData.class);
  private static final Random random = new Random();
  private final Environment environment;
  private final UserRepository userRepository;
  private final ItemRepository itemRepository;
  private final OrderRepository orderRepository;
  private final OrderService orderService;
  private final PasswordEncoder passwordEncoder;
  private final OrderItemRepository orderItemRepository;

  public FakeData(Environment environment, UserRepository userRepository, ItemRepository itemRepository, OrderRepository orderRepository, OrderItemRepository orderItemRepository, OrderService orderService) {
    this.environment = environment;
    this.orderService = orderService;
    this.userRepository = userRepository;
    this.itemRepository = itemRepository;
    this.orderRepository = orderRepository;
    this.orderItemRepository = orderItemRepository;
    // TODO: centralize password encoder
    this.passwordEncoder = new BCryptPasswordEncoder();
  }

  @EventListener
  public void seedFakeDb(ContextRefreshedEvent event) {
    if (environment.containsProperty("fake.data.seed") && Boolean.parseBoolean(environment.getProperty("fake.data.seed"))) {
      logger.info("Adding fake data...");
      seedFakeStoreApiUsers();
      seedFakeStoreApiItems();
      seedFakeStoreApiOrders();
    }
  }

  public void seedFakeStoreApiUsers() {
    if (userRepository.findAll().size() > 10) {
      logger.info("Users already exist in database");
      return;
    }

    logger.info("Loading fake users from StoreApi");

    HttpClient client = HttpClient.newBuilder()
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(20))
      .build();

    HttpRequest request = HttpRequest.newBuilder()
      .uri(URI.create("https://fakestoreapi.com/users"))
      .timeout(Duration.ofMinutes(2))
      .header("Content-Type", "application/json")
      .GET()
      .build();

    client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
      .thenApply(HttpResponse::body)
      .thenAccept(body -> {
        JsonParser springParser = JsonParserFactory.getJsonParser();
        List<Object> apiUsers = springParser.parseList(body);
        int addUserCount = 0;
        for (Object user : apiUsers) {
          try {
            UserEntity newUser = new UserEntity();
            newUser.setEmail((String) ((Map<String, Object>) user).get("email"));
            newUser.setFirstName((String) ((Map<String, Object>) ((Map<String, Object>) user).get("name")).get("firstname"));
            newUser.setLastName((String) ((Map<String, Object>) ((Map<String, Object>) user).get("name")).get("lastname"));
            newUser.setPassword(passwordEncoder.encode((String) ((Map<String, Object>) user).get("password")));
            Map<String, Object> addressMap = (Map<String, Object>) ((Map<String, Object>) user).get("address");
            newUser.setAddress(addressMap.get("street") + " " + addressMap.get("number") + ", " + addressMap.get("zipcode") + " " + addressMap.get("city"));
            newUser.setPhone((String) ((Map<String, Object>) user).get("phone"));
            newUser.setIsAdmin(false);
            logger.info("Adding user: {}", newUser.getFirstName());
            userRepository.save(newUser);
            addUserCount++;
          } catch (Exception e) {
            logger.error("Failed to process user: {}", user, e);
          }
        }
        logger.info("Added {} users", addUserCount);
      });
  }

  public void seedFakeStoreApiItems() {
    if (itemRepository.findAll().size() > 10) {
      logger.info("Items already exist in database");
      return;
    }

    logger.info("Loading fake items from StoreApi");

    HttpClient client = HttpClient.newBuilder()
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(20))
      .build();

    HttpRequest request = HttpRequest.newBuilder()
      .uri(URI.create("https://fakestoreapi.com/products"))
      .timeout(Duration.ofMinutes(2))
      .header("Content-Type", "application/json")
      .GET()
      .build();

    client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
      .thenApply(HttpResponse::body)
      .thenAccept(body -> {
        JsonParser springParser = JsonParserFactory.getJsonParser();
        List<Object> apiItems = springParser.parseList(body);
        logger.debug("Parsed {} items", apiItems.size());
        int addedCount = 0;
        for (Object item : apiItems) {
          try {
            ItemEntity newItem = new ItemEntity();
            newItem.setName((String) ((Map<String, Object>) item).get("title"));
            newItem.setPrice((float) ((Double) ((Map<String, Object>) item).get("price")).doubleValue());
            newItem.setCategory((String) ((Map<String, Object>) item).get("category"));
            newItem.setDescription((String) ((Map<String, Object>) item).get("description"));
            int stock = (int) Math.floor(random.nextGaussian() * 100);
            if (stock < 0) {
              stock *= -1;
            }
            newItem.setStock(stock);
            newItem.setImage((String) ((Map<String, Object>) item).get("image"));

            logger.info("Adding item: {}", newItem.getName());
            itemRepository.save(newItem);
            addedCount++;
          } catch (Exception e) {
            logger.warn("Failed to process item: {}", item, e);
          }
        }
        logger.info("Added {} items to database", addedCount);
      });
  }

  public void seedFakeStoreApiOrders() {
    if (orderRepository.findAll().size() > 15) {
      logger.info("Orders already exist in database");
      return;
    }

    logger.info("Creating fake orders for existing users");

    try {
      List<UserEntity> users = null;
      List<ItemEntity> items = null;
      while (users == null || items == null || users.size() < 10 || items.size() < 10) {
        try {
          Thread.sleep(5000);
          logger.info("Waiting for 5 seconds until users and items are be created...");
        } catch (InterruptedException e) {
        }
        users = userRepository.findAll();
        items = itemRepository.findAll();
      }

      logger.info("Users are created, creating orders...");

      int addOrderCount = 0;
      for (UserEntity user : users) {
        OrderEntity newOrder = new OrderEntity();
        OrderItemEntity item = new OrderItemEntity();
        List<OrderItemEntity> itemsInOrder = new ArrayList<>();
        logger.info("added order {} ", newOrder.getId());
        newOrder.setCustomer(user);
        newOrder.setCustomerFirstName(user.getFirstName());
        newOrder.setCustomerLastName(user.getLastName());
        newOrder.setCustomerAddress(user.getAddress());
        newOrder.setCustomerPhone(user.getPhone());
        newOrder.setOrderStatus(randomEnumGenerator());
        UUID itemId = setItemToCart(newOrder).getItemId();
        item.setItem(itemRepository.getById(itemId));
        item.setOrder(newOrder);
        item.setQuantity(setItemToCart(newOrder).getQuantity());
        itemsInOrder.add(item);
        newOrder.setItems(itemsInOrder);
        orderRepository.save(newOrder);
        orderService.addItemToCart(setItemToCart(newOrder));


        logger.info("item x: {}", newOrder.getItems());
        addOrderCount++;
      }

      logger.info("Added {} orders to database", addOrderCount);
    } catch (Exception e) {
      logger.error("Failed to generate order", e);
    }
  }

  /**
   * Method for returning a AddItemToCartRequest
   *
   * @param newOrder - orderentity you want to add items to
   * @return cartRequest - AddItemToCartRequest with random item / quantity and user/order from newOrder
   */

  public AddItemToCartRequest setItemToCart(OrderEntity newOrder) {
    AddItemToCartRequest cartRequest = new AddItemToCartRequest();
    int min = 1;
    int max = 4;
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    for (int i = 0; i < itemRepository.findAll().size(); i++) {
      cartRequest.setItemId(itemRepository.findAll().get(i).getId());
      if (itemRepository.findAll().get(i).getStock() >= randomNum) {
        cartRequest.setQuantity(randomNum);
      } else {
        cartRequest.setQuantity(0);
      }
    }
    cartRequest.setCustomerId(newOrder.getCustomer().getId());
    return cartRequest;
  }

  /**
   * Method for returning a random OrderStatusEnum
   *
   * @return random enum - CART, ORDERED, SHIPPED
   */

  public OrderStatusEnum randomEnumGenerator() {
    OrderStatusEnum en = OrderStatusEnum.CART;
    int min = 1;
    int max = 2;
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    if (randomNum == 1) {
      en = OrderStatusEnum.ORDERED;
      return en;
    }
    if (randomNum == 2) {
      en = OrderStatusEnum.SHIPPED;
    }
    return en;
  }
}
