package com.eShop.backend.controller;

import com.eShop.backend.dto.UserDto;
import com.eShop.backend.mapper.UserMapper;
import com.eShop.backend.model.UserEntity;
import com.eShop.backend.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

/**
 * Todo:
 * Authorization for DeleteRequest
 * GetRequest should only return name and lastname?
 */

@RestController
@RequestMapping(path = "/api/users")
public class UserController {

  private final IUserService userService;
  private final UserMapper userMapper;

  @Autowired
  public UserController(IUserService userService, UserMapper userMapper) {
    this.userService = userService;
    this.userMapper = userMapper;
  }

  @GetMapping(path = "all")
  public Iterable<UserEntity> getAllUsers() {
    return this.userService.getAllUsers();
  }

  @GetMapping(path = "{id}")
  public Optional<UserDto> getUserById(@PathVariable UUID id) {
    Optional<UserEntity> user = this.userService.getUserById(id);
    return user.map(userMapper::toDto);
  }

  @PostMapping(path = "add")
  public void createUser(@RequestBody UserEntity user) {
    this.userService.saveUser(user);
  }

  @DeleteMapping(value = "delete/{id}")
  public ResponseEntity<UUID> deleteUser(@PathVariable UUID id) {
    this.userService.deleteUser(id);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }
}
