package com.eShop.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldController.class);

  public HelloWorldController() {
  }

  @GetMapping("/")
  public ResponseEntity<String> pong() {
    LOGGER.info("Ponging a ping");
    return ResponseEntity.ok("pong");
  }
}
