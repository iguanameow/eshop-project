package com.eShop.backend.controller;

import com.eShop.backend.dto.AdminItemResponse;
import com.eShop.backend.dto.ItemDto;
import com.eShop.backend.service.IItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/items")
public class ItemController {
  private final IItemService itemService;

  public ItemController(IItemService itemService) {
    this.itemService = itemService;
  }

  @GetMapping
  public List<ItemDto> readItems() {
    return itemService.getItems();
  }

  @GetMapping(path = "/{itemId}")
  public ItemDto readItem(@PathVariable(value = "itemId") UUID id) {
    return itemService.getItem(id);
  }

  @PostMapping
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<AdminItemResponse> createItem(@RequestBody ItemDto item) {
    return itemService.createItem(item);
  }

  @PutMapping(path = "/{itemId}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<AdminItemResponse> updateItem(@PathVariable(value = "itemId") UUID id,
                                                      @RequestBody ItemDto item) {
    return itemService.updateItem(id, item);
  }

  @DeleteMapping(path = "/{itemId}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<AdminItemResponse> deleteItem(@PathVariable(value = "itemId") UUID id) {
    return itemService.deleteItem(id);
  }
}
