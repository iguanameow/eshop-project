package com.eShop.backend.controller;

import com.eShop.backend.dto.*;
import com.eShop.backend.mapper.OrderMapper;
import com.eShop.backend.model.OrderEntity;
import com.eShop.backend.model.UserEntity;
import com.eShop.backend.service.IOrderService;
import com.eShop.backend.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {
  private static final Logger LOGGER = LoggerFactory.getLogger(IOrderService.class);
  private final IOrderService orderService;
  private final OrderMapper orderMapper;
  private final IUserService userService;

  @Autowired
  public OrderController(IOrderService orderService, OrderMapper orderMapper, IUserService userService) {
    this.orderService = orderService;
    this.orderMapper = orderMapper;
    this.userService = userService;
  }

  @GetMapping
  public List<OrderEntity> readOrders() {
    return orderService.getOrders();
  }

  @GetMapping(path = "{orderId}")
  public Optional<OrderDto> getOrder(@PathVariable("orderId") UUID orderId) {
    Optional<OrderEntity> orderEntity = orderService.getOrderById(orderId);
    return orderEntity.map(orderMapper::toDto);
  }

  @PostMapping("add")
  public String createOrder(@RequestBody OrderEntity orderEntity) {
    orderService.createOrder(orderEntity);
    return "Order Created!";
  }

  @PutMapping(path = "status/{orderId}")
  public OrderEntity orderStatus(@PathVariable(value = "orderId") UUID id, @RequestBody OrderEntity orderEntity) {
    LOGGER.info("Updated order status to: ({}) for order id: ({})", orderEntity.getOrderStatus(), id);
    return orderService.updateOrderStatus(id, orderEntity.getOrderStatus());
  }

  @PutMapping("{orderId}")
  public boolean updateOrder(@PathVariable UUID orderId, @RequestBody OrderDto orderDto) {
    if (!orderId.equals(orderDto.getId())) {
      return false;
    }
    Optional<UserEntity> user = userService.getUserById(orderDto.getCustomerId());
    if (user.isPresent()) {
      OrderEntity orderEntity = orderMapper.toEntity(orderDto);
      orderEntity.setCustomer(user.get());
      return orderService.updateOrder(orderEntity);
    } else {
      return false;
    }

  }

  @GetMapping("cart")
  public Optional<OrderDto> getCart(@RequestParam("userId") UUID userId) {
    Optional<OrderEntity> orderEntity = orderService.getCartForUser(userId);
    return orderEntity.map(orderMapper::toDto);
  }

  @PostMapping("cart")
  public AddItemToCartResponse addItemToCart(@RequestBody AddItemToCartRequest addItemToCartRequest) {
    return orderService.addItemToCart(addItemToCartRequest);
  }

  @DeleteMapping(path = "cart/{orderItemId}")
  public void deleteItem(@PathVariable(value = "orderItemId") UUID orderItemId) {
    orderService.removeItemFromCart(orderItemId);
  }

  @PostMapping("complete/{orderId}")
  public boolean updateOrderStatusToPaid(@PathVariable(value = "orderId") UUID orderId) {
    return orderService.updateOrderStatusToPaid(orderId);
  }


  @RequestMapping("count")
  public OrderItemsCountResponse orderItemsCount(OrderItemsCountRequest itemsCountDto) {
    return orderService.getOrderItemsCount(itemsCountDto);
  }

}
