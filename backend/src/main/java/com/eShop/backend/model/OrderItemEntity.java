package com.eShop.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class OrderItemEntity extends BaseEntity {

  private int quantity;

  @ManyToOne(targetEntity = OrderEntity.class, optional = false)
  @JsonBackReference
  private OrderEntity order;

  @ManyToOne(targetEntity = ItemEntity.class, optional = false)
  private ItemEntity item;

  public ItemEntity getItem() {
    return this.item;
  }

  public void setItem(ItemEntity itemId) {
    this.item = itemId;
  }

  public int getQuantity() {
    return this.quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Override
  public String toString() {
    return "{" +
      " itemId='" + getItem() + "'" +
      ", quantity='" + getQuantity() + "'" +
      "}";
  }


  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (!(o instanceof OrderItemEntity)) {
      return false;
    }
    OrderItemEntity orderItemEntity = (OrderItemEntity) o;
    return Objects.equals(item, orderItemEntity.item) && quantity == orderItemEntity.quantity;
  }

  public OrderEntity getOrder() {
    return order;
  }

  public void setOrder(OrderEntity order) {
    this.order = order;
  }
}
