package com.eShop.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderStatusEnum {
  @JsonProperty("0")
  CART,
  @JsonProperty("1")
  ORDERED,
  @JsonProperty("2")
  SHIPPED
}
