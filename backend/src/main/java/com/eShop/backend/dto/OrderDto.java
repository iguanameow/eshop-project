package com.eShop.backend.dto;

import com.eShop.backend.model.OrderItemEntity;
import com.eShop.backend.model.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
  @JsonProperty("id")
  private UUID id;

  @JsonProperty("items")
  private List<OrderItemEntity> items;

  @JsonProperty("customerId")
  private UUID customerId;

  @JsonProperty("orderStatus")
  private OrderStatusEnum orderStatus;

  @JsonProperty("createdAt")
  private Timestamp createdAt;

  @JsonProperty("customerFirstName")
  private String customerFirstName;

  @JsonProperty("customerLastName")
  private String customerLastName;

  @JsonProperty("customerAddress")
  private String customerAddress;

  @JsonProperty("customerPhone")
  private String customerPhone;

  @JsonProperty("totalPrice")
  private float totalPrice;

  public UUID getId() {
    return this.id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public List<OrderItemEntity> getItems() {
    return this.items;
  }

  public void setItems(List<OrderItemEntity> items) {
    this.items = items;
  }

  public UUID getCustomerId() {
    return this.customerId;
  }

  public void setCustomerId(UUID customerId) {
    this.customerId = customerId;
  }

  public OrderStatusEnum getOrderStatus() {
    return this.orderStatus;
  }

  public void setOrderStatus(OrderStatusEnum orderStatus) {
    this.orderStatus = orderStatus;
  }

  public Timestamp getCreatedAt() {
    return this.createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public String getCustomerFirstName() {
    return this.customerFirstName;
  }

  public void setCustomerFirstName(String customerFirstName) {
    this.customerFirstName = customerFirstName;
  }

  public String getCustomerLastName() {
    return this.customerLastName;
  }

  public void setCustomerLastName(String customerLastName) {
    this.customerLastName = customerLastName;
  }

  public String getCustomerAddress() {
    return this.customerAddress;
  }

  public void setCustomerAddress(String customerAddress) {
    this.customerAddress = customerAddress;
  }

  public String getCustomerPhone() {
    return this.customerPhone;
  }

  public void setCustomerPhone(String customerPhone) {
    this.customerPhone = customerPhone;
  }

  public float getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(float totalPrice) {
    this.totalPrice = totalPrice;
  }
}
