package com.eShop.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminItemResponse {
  @JsonProperty("statusCode")
  private int statusCode;
  @JsonProperty("message")
  private String message;
}
