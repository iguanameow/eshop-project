package com.eShop.backend.dto;

public class OrderItemsCountResponse {
  private Boolean requestStatus;
  private Integer count;

  public OrderItemsCountResponse(boolean requestStatus, int count) {
    this.requestStatus = requestStatus;
    this.count = count;
  }

  public OrderItemsCountResponse() {
  }

  public Boolean getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(Boolean requestStatus) {
    this.requestStatus = requestStatus;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}
