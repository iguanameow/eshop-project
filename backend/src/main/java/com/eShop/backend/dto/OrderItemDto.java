package com.eShop.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {
  @JsonProperty("itemId")
  private UUID itemId;

  @JsonProperty("quantity")
  private int quantity;


  public UUID getItemId() {
    return this.itemId;
  }

  public void setItemId(UUID itemId) {
    this.itemId = itemId;
  }

  public int getQuantity() {
    return this.quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

}
