package com.eShop.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class RemoveCartItemRequest {

  @JsonProperty("itemId")
  UUID itemId;

  @JsonProperty("orderItemId")
  UUID orderItemId;

  @JsonProperty("orderId")
  UUID orderId;


  public UUID getItemId() {
    return itemId;
  }

  public void setItemId(UUID itemId) {
    this.itemId = itemId;
  }

  public UUID getOrderItemId() {
    return orderItemId;
  }

  public void setOrderItemId(UUID orderItemId) {
    this.orderItemId = orderItemId;
  }

  public UUID getOrderId() {
    return orderId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }
}
