package com.eShop.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemsCountRequest {
  @JsonProperty("orderId")
  private UUID orderId;

  @JsonProperty("userId")
  private UUID userId;

  public UUID getOrderId() {
    return orderId;
  }

  public UUID getUserId() {
    return userId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }
}
