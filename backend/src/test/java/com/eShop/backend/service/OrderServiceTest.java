package com.eShop.backend.service;

import com.eShop.backend.dto.AddItemToCartRequest;
import com.eShop.backend.dto.AddItemToCartResponse;
import com.eShop.backend.dto.OrderItemsCountRequest;
import com.eShop.backend.dto.OrderItemsCountResponse;
import com.eShop.backend.model.*;
import com.eShop.backend.repository.ItemRepository;
import com.eShop.backend.repository.OrderItemRepository;
import com.eShop.backend.repository.OrderRepository;
import com.eShop.backend.repository.UserRepository;
import com.eShop.backend.service.impl.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Transactional
public class OrderServiceTest {
  @Resource
  private OrderRepository orderRepository;
  @Mock
  private OrderRepository orderRepositoryMock;
  private OrderService orderUnderTest;
  private OrderService orderUnderTestMock;
  private OrderEntity order;
  @Resource
  private ItemRepository itemRepository;
  @Mock
  private ItemRepository itemRepositoryMock;
  private ItemEntity item;
  private ItemEntity item2;
  @Resource
  private UserRepository userRepository;
  @Mock
  private UserRepository userRepositoryMock;
  private UserEntity user;
  @Resource
  private OrderItemRepository orderItemRepository;
  @Mock
  private OrderItemRepository orderItemRepositoryMock;
  private OrderItemEntity orderItem;

  @BeforeEach
  void setup() {
    orderUnderTest = new OrderService(orderRepository, itemRepository, orderItemRepository, userRepository);
    orderUnderTestMock = new OrderService(orderRepositoryMock, itemRepositoryMock, orderItemRepositoryMock, userRepositoryMock);
    order = new OrderEntity();
    user = new UserEntity();
    item = new ItemEntity();
    item2 = new ItemEntity();
    orderItem = new OrderItemEntity();

    user.setEmail("mail@a.com");
    user.setFirstName("first");
    user.setLastName("second");
    user.setPassword("123456");
    user.setPhone("1111111");
    user.setIsAdmin(false);
    user.setAddress("road 1");
    item.setName("item1");
    item.setCategory("category1");
    item.setDescription("description1");
    item.setImage("src/image1.png");
    item.setPrice((float) 11.1);
    item.setStock(11);

    item2.setName("item2");
    item2.setCategory("category2");
    item2.setDescription("description2");
    item2.setImage("src/image2.png");
    item2.setPrice((float) 29.9);
    item2.setStock(8);

    orderItem.setItem(item);
    orderItem.setOrder(order);
    orderItem.setQuantity(1);

    List<OrderItemEntity> itemsInOrder = new ArrayList<>();
    itemsInOrder.add(orderItem);

    order.setCustomer(user);
    order.setCustomerAddress(user.getAddress());
    order.setCustomerFirstName(user.getFirstName());
    order.setCustomerLastName(user.getLastName());
    order.setCustomerPhone(user.getPhone());
    order.setItems(itemsInOrder);
    order.setOrderStatus(OrderStatusEnum.CART);
  }

  @Test
  @DisplayName("Testing an actual order to see if getCartForUser() works. Should return true")
  void itShouldGetUsersCart() {
    orderUnderTestMock.createOrder(order);
    Optional<OrderEntity> orderFound = orderUnderTestMock.getCartForUser(order.getCustomer().getId());
    orderFound.ifPresent(orderEntity -> assertEquals(orderFound, order));
  }

  @Test
  @DisplayName("Testing an user with no active order to check if cart is empty")
  void itShouldGetEmptyCart() {
    UserEntity user = new UserEntity();
    Optional<OrderEntity> createdOrder = orderUnderTest.getCartForUser(user.getId());
    assertThat(createdOrder.isEmpty());
  }

  @Test
  @DisplayName("Test should pass if the createOrder method in OrderService works properly")
  void itShouldCreateOrder() {
    orderUnderTestMock.createOrder(order);
    ArgumentCaptor<OrderEntity> orderEntityArgumentCaptor = ArgumentCaptor.forClass(OrderEntity.class);
    verify(orderRepositoryMock).save(orderEntityArgumentCaptor.capture());
    OrderEntity capturedOrder = orderEntityArgumentCaptor.getValue();
    assertEquals(capturedOrder, order);
  }

  @Test
  @DisplayName("Test if orderId same as the active order for a user (create a new one if doesn't exist)")
  void itShouldGetUserOrderID() {
    orderUnderTestMock.getCustomerOrderIdOrCreate(user.getId());
    verify(userRepositoryMock).findById(orderUnderTestMock.getCustomerOrderIdOrCreate(user.getId()));
  }

  @Test
  @DisplayName("Test should pass if all orders are returned")
  void itShouldGetAllOrders() {
    orderUnderTestMock.getOrders();
    verify(orderRepositoryMock).findAll();
  }

  @Test
  @DisplayName("When the order ID exists the method getOrderItems count should return a OrderItemsCountResponse with request status true and a count equal to the number of items in the order")
  void orderItemCountYesOrderYesUser() {
    OrderItemsCountRequest request = new OrderItemsCountRequest();
    OrderItemsCountResponse actualResponse = new OrderItemsCountResponse(true, 1);
    request.setOrderId(order.getId());
    request.setUserId(user.getId());
    Mockito.when(orderRepositoryMock.getById(request.getOrderId())).thenReturn(order);
    System.out.println(order.getItems().size());
    assertEquals(orderUnderTestMock.getOrderItemsCount(request).getCount(), actualResponse.getCount());
    assertEquals(orderUnderTestMock.getOrderItemsCount(request).getRequestStatus(), actualResponse.getRequestStatus());

  }

  @Test
  @DisplayName("Hello")
  void orderItemCountNoOrderYesUser() {
    OrderItemsCountRequest request = new OrderItemsCountRequest();
    OrderItemsCountResponse response = new OrderItemsCountResponse(true, 1);
    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setCustomer(user);
    Optional<OrderEntity> mockEntity = Optional.of(orderEntity);
    request.setUserId(mockEntity.get().getCustomer().getId());
    //assertEquals(response.getCount(), orderUnderTest.getOrderItemsCount(request).getCount());
  }

  @Test
  @DisplayName("If the getOrderId = null and the getUserID = null the method should return a orderItemsResponse with value (true, 0)")
  void orderItemCountNoOrderNoUser() {
    OrderItemsCountRequest nullRequest = new OrderItemsCountRequest();
    OrderItemsCountResponse nullResponse = new OrderItemsCountResponse(true, 0);
    nullRequest.setUserId(null);
    nullRequest.setOrderId(null);
    assertEquals(orderUnderTest.getOrderItemsCount(nullRequest).getCount(), nullResponse.getCount());
    assertEquals(orderUnderTest.getOrderItemsCount(nullRequest).getRequestStatus(), nullResponse.getRequestStatus());
  }

  @Test
  @DisplayName("If getOrderID = null, getUserID != null but the orderEntity.IsPresent() = false it should return (false, 0)")
  void orderItemCountNoOrderYesUserFalseIsPresent() {
    OrderItemsCountRequest request = new OrderItemsCountRequest();
    OrderItemsCountResponse response = new OrderItemsCountResponse(false, 0);
    request.setOrderId(null);
    request.setUserId(user.getId());

    assertEquals(response.getCount(), orderUnderTest.getOrderItemsCount(request).getCount());
    assertEquals(response.getRequestStatus(), orderUnderTest.getOrderItemsCount(request).getRequestStatus());
  }

  @Test
  @DisplayName("addItemToCart: No active order but user available -> will active getCustomerOrderIdOrCreate for the user which creates a new order -> the response should  be true, (item added to cart)")
  void addItemNoOrderYesUser() {
    AddItemToCartRequest sampleRequest = new AddItemToCartRequest();
    AddItemToCartResponse actualResponse = new AddItemToCartResponse();
    actualResponse.setRequestStatus(true);
    actualResponse.setMessage("Item added to cart: quantity updated");
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);
    sampleRequest.setCustomerId(user.getId());
    sampleRequest.setQuantity(1);
    sampleRequest.setItemId(item.getId());
    assertEquals(actualResponse.getRequestStatus(), orderUnderTest.addItemToCart(sampleRequest).getRequestStatus());
    assertEquals(actualResponse.getMessage(), orderUnderTest.addItemToCart(sampleRequest).getMessage());
    AddItemToCartRequest addItemToCartRequest = new AddItemToCartRequest();
    addItemToCartRequest.setOrderId(null);
    addItemToCartRequest.setCustomerId(user.getId());
  }


  @Test
  @DisplayName("The order exists and the user exists but the request has a quantity that is to high -> response should be false (Out of stock!)")
  void addItemOrderExistNoStock() {

    AddItemToCartRequest sampleRequest = new AddItemToCartRequest();
    AddItemToCartResponse actualResponse = new AddItemToCartResponse();
    actualResponse.setMessage("Out of stock!");
    actualResponse.setRequestStatus(false);
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);
    sampleRequest.setCustomerId(user.getId());
    sampleRequest.setQuantity(200);
    sampleRequest.setItemId(item.getId());
    orderUnderTest.addItemToCart(sampleRequest);
    item.setStock(0);
    orderUnderTest.createOrder(order);

    assertEquals(actualResponse.getMessage(), orderUnderTest.addItemToCart(sampleRequest).getMessage());
    assertEquals(actualResponse.getRequestStatus(), orderUnderTest.addItemToCart(sampleRequest).getRequestStatus());
  }

  @Test
  void addItemUpdateItemAlreadyInCart() {
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);
    AddItemToCartRequest addItemToCartRequest = new AddItemToCartRequest();
    addItemToCartRequest.setCustomerId(user.getId());
    addItemToCartRequest.setOrderId(order.getId());
    addItemToCartRequest.setItemId(item.getId());
    addItemToCartRequest.setQuantity(orderItem.getQuantity());
    assertTrue(orderUnderTest.addItemToCart(addItemToCartRequest).getRequestStatus()); //2
    assertEquals("Item added to cart: quantity updated", orderUnderTest.addItemToCart(addItemToCartRequest).getMessage()); //3
    assertEquals(orderItemRepository.getById(orderItem.getId()).getQuantity(), 3);

  }

  @Test
  void addItemYesOrderNotRepository() {
    userRepository.save(user);
    itemRepository.save(item);
    AddItemToCartRequest addItemToCartRequest = new AddItemToCartRequest();
    addItemToCartRequest.setCustomerId(user.getId());
    addItemToCartRequest.setOrderId(order.getId());
    addItemToCartRequest.setItemId(item.getId());
    addItemToCartRequest.setQuantity(orderItem.getQuantity());
    assertFalse(orderUnderTest.addItemToCart(addItemToCartRequest).getRequestStatus());
    assertEquals("Order doesn't exist!", orderUnderTest.addItemToCart(addItemToCartRequest).getMessage());

  }

  @Test
  void addItemNewItemAdded() {
    userRepository.save(user);
    itemRepository.save(item);
    itemRepository.save(item2);
    orderRepository.save(order);
    AddItemToCartRequest addItemToCartRequest = new AddItemToCartRequest();
    addItemToCartRequest.setCustomerId(user.getId());
    addItemToCartRequest.setOrderId(order.getId());
    addItemToCartRequest.setItemId(item2.getId());
    addItemToCartRequest.setQuantity(orderItem.getQuantity());

    assertEquals("Item added to cart", orderUnderTest.addItemToCart(addItemToCartRequest).getMessage());
    assertEquals(orderRepository.getById(order.getId()).getItems().size(), 2);
  }


  @Test
  void removeItemCheckItemRemovedFromRepository() {

    // adding item to cart
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);
    AddItemToCartRequest addItemToCartRequest = new AddItemToCartRequest();
    addItemToCartRequest.setCustomerId(user.getId());
    addItemToCartRequest.setOrderId(order.getId());
    addItemToCartRequest.setItemId(item.getId());
    addItemToCartRequest.setQuantity(orderItem.getQuantity());
    orderUnderTest.addItemToCart(addItemToCartRequest);

    // removing the item
    orderUnderTest.removeItemFromCart(orderItem.getId());

    assertFalse(orderItemRepository.existsById(orderItem.getItem().getId()));
  }

  @Test
  void removeItemCheckItemQuantityUpdated() {

    // adding item to cart
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);

    // removing the item
    orderUnderTest.removeItemFromCart(orderItem.getId());

    assertEquals(itemRepository.getById(item.getId()).getStock(), 12);
  }

  @Test
  void getOrderByIdShouldReturnOrder() {
    // arrange
    OrderEntity order = new OrderEntity();
    order.setId(UUID.fromString("7fd5c7ee-107b-4939-8c41-b43c3ce5ef5a"));
    orderRepository.save(order);

    // act
    Optional<OrderEntity> result = orderUnderTest.getOrderById(order.getId());

    // assert
    assertNotNull(result);
    assertTrue(result.isPresent());
    assertEquals(order.getId(), result.get().getId());
  }

  @Test
  void getOrderByIdShouldReturnNull() {
    // arrange
    OrderEntity order = new OrderEntity();
    order.setId(UUID.fromString("7fd5c7ee-107b-4939-8c41-b43c3ce5ef5a"));
    orderRepository.save(order);

    // act
    Optional<OrderEntity> result = orderUnderTest.getOrderById(UUID.fromString("9eb8f9ed-9109-4147-adc4-b5cdb9dea6c2"));

    // assert
    assertTrue(result.isEmpty());
  }

  @Test
  @DisplayName("Test should pass if the updateOrder method in OrderService works properly")
  void updateOrderShouldReturnTrue() {
    userRepository.save(user);
    itemRepository.save(item);
    orderRepository.save(order);
    assertTrue(orderUnderTest.updateOrder(order));
  }

  @Test
  @DisplayName("Test should pass if the updateOrder method in OrderService works properly")
  void updateOrderShouldReturnFalse() {
    OrderEntity nullOrder = new OrderEntity();
    assertFalse(orderUnderTest.updateOrder(nullOrder));
  }

}
