package com.eShop.backend.service;

import com.eShop.backend.model.UserEntity;
import com.eShop.backend.repository.UserRepository;
import com.eShop.backend.service.impl.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Transactional
public class UserServiceTest {

  @Mock
  private UserRepository userRepositoryMock;
  @Resource
  private UserRepository userRepositoryInMemory;
  private UserService underTestMockDb;
  private UserService underTestInMemoryDb;
  private UserEntity user;

  @BeforeEach
  void setUp() {
    underTestMockDb = new UserService(userRepositoryMock);
    underTestInMemoryDb = new UserService(userRepositoryInMemory);

    user = new UserEntity();
    user.setEmail("mail1");
    user.setFirstName("fname1");
    user.setLastName("lname1");
    user.setPassword("password1");
    user.setAddress("address1");
    user.setPhone("phone1");
    user.setIsAdmin(false);
  }

  @Test
  void itShouldGetAllUsers() {
    underTestMockDb.getAllUsers();
    verify(userRepositoryMock).findAll();
  }

  @Test
  void itShouldCreateAUser() {
    underTestMockDb.saveUser(user);

    ArgumentCaptor<UserEntity> userArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

    verify(userRepositoryMock).save(userArgumentCaptor.capture());

    UserEntity capturedUser = userArgumentCaptor.getValue();

    assertThat(capturedUser).isEqualTo(user);
  }

  @Test
  void itShouldRemoveAUser() {
    underTestMockDb.deleteUser(user.getId());

    verify(userRepositoryMock).deleteById(user.getId());
  }


  @Test
  void getUserByIdShouldReturnOrder() {
    // arrange
    UserEntity userEntity = new UserEntity();
    userEntity.setId(UUID.fromString("7fd5c7ee-107b-4939-8c41-b43c3ce5ef5a"));
    userRepositoryInMemory.save(userEntity);

    // act
    Optional<UserEntity> result = underTestInMemoryDb.getUserById(userEntity.getId());

    // assert
    assertNotNull(result);
    assertTrue(result.isPresent());
    assertEquals(userEntity.getId(), result.get().getId());
  }

  @Test
  void getUserByIdShouldReturnNull() {
    // arrange
    UserEntity userEntity = new UserEntity();
    userEntity.setId(UUID.fromString("7fd5c7ee-107b-4939-8c41-b43c3ce5ef5a"));
    userRepositoryInMemory.save(userEntity);

    // act
    Optional<UserEntity> result = underTestInMemoryDb.getUserById(UUID.fromString("9eb8f9ed-9109-4147-adc4-b5cdb9dea6c2"));

    // assert
    assertTrue(result.isEmpty());
  }

}
