package com.eShop.backend.service;

import com.eShop.backend.repository.OrderItemRepository;
import com.eShop.backend.service.impl.OrderItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class OrderItemServiceTest {
  @Mock
  OrderItemRepository orderItemRepository;
  OrderItemService orderItemUnderTest;

  @BeforeEach
  void setup() {
    orderItemUnderTest = new OrderItemService(orderItemRepository);
  }

  @Test
  void itShouldCreateOrderItemService() {
    OrderItemService service = new OrderItemService(orderItemRepository);
    if (service == null) {
      boolean bool = false;
      assertThat(false);
    }
    boolean bool = true;
    assertThat(bool = true);
  }

}
