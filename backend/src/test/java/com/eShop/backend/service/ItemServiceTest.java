package com.eShop.backend.service;

import com.eShop.backend.dto.ItemDto;
import com.eShop.backend.mapper.ItemMapper;
import com.eShop.backend.model.ItemEntity;
import com.eShop.backend.repository.ItemRepository;
import com.eShop.backend.service.impl.ItemService;
import com.eShop.backend.service.impl.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ItemServiceTest {

  @Mock
  private ItemRepository itemRepository;
  @Mock
  private OrderService orderService;
  private ItemMapper itemMapper;
  private ItemService underTest;
  private ItemDto item;

  @BeforeEach
  void setUp() {
    itemMapper = new ItemMapper();
    underTest = new ItemService(itemRepository, orderService, itemMapper);
    item = new ItemDto();
    item.setName("item1");
    item.setCategory("category1");
    item.setDescription("description1");
    item.setImage("src/image1.png");
    item.setPrice((float) 11.1);
    item.setStock(11);
  }

  @Test
  void itShouldGetAllItems() {
    List<ItemEntity> expected = itemRepository.findAll();
    List<ItemEntity> actual = itemMapper.toEntity(underTest.getItems());

    assertEquals(expected, actual);
  }

  @Test
  void itShouldGetAnItem() {
    item.setId(UUID.randomUUID());

    when(itemRepository.findById(item.getId())).thenReturn(Optional.of(itemMapper.toEntity(item)));

    assertEquals(item, underTest.getItem(item.getId()));
  }

  @Test
  void itShouldCreateAnItem() {
    underTest.createItem(item);

    ArgumentCaptor<ItemEntity> itemArgumentCaptor = ArgumentCaptor.forClass(ItemEntity.class);

    verify(itemRepository).save(itemArgumentCaptor.capture());

    ItemEntity capturedItem = itemArgumentCaptor.getValue();
    item.setId(capturedItem.getId());

    assertThat(itemMapper.toDto(capturedItem)).isEqualTo(item);
  }

  @Test
  void itShouldUpdateAnItem() {
    ItemDto item2 = new ItemDto();
    item2.setName("item2");
    item2.setCategory("category2");
    item2.setDescription("description2");
    item2.setImage("src/image2.png");
    item2.setPrice((float) 22.2);
    item2.setStock(22);

    when(itemRepository.findById(item.getId())).thenReturn(Optional.of(itemMapper.toEntity(item)));

    underTest.updateItem(item.getId(), item2);

    ArgumentCaptor<ItemEntity> itemArgumentCaptor = ArgumentCaptor.forClass(ItemEntity.class);

    verify(itemRepository).save(itemArgumentCaptor.capture());

    ItemEntity capturedItem = itemArgumentCaptor.getValue();
    item2.setId(capturedItem.getId());

    assertThat(itemMapper.toDto(capturedItem)).isEqualTo(item2);
  }

  @Test
  void itShouldNotUpdateAnItemIfStockIsNegative() {
    ItemDto item2 = new ItemDto();
    item2.setName("item2");
    item2.setCategory("category2");
    item2.setDescription("description2");
    item2.setImage("src/image2.png");
    item2.setPrice((float) 22.2);
    item2.setStock(-10);

    when(itemRepository.findById(item.getId())).thenReturn(Optional.of(itemMapper.toEntity(item)));

    underTest.updateItem(item.getId(), item2);

    ItemDto actualItem = underTest.getItem(item.getId());

    assertThat(actualItem).isNotEqualTo(item2);
  }

  @Test
  void itShouldRemoveAnItem() {
    underTest.deleteItem(item.getId());
    verify(itemRepository).deleteById(item.getId());
  }
}
