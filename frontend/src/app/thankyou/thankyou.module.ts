import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from "../core/core.module";
import {UserRoutingModule} from './thankyou-routing.module';
import {UserThankyouComponent} from './thankyou-view/thankyou.component';

@NgModule({
  declarations: [
    UserThankyouComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    CoreModule
  ],
  exports: [
    UserThankyouComponent
  ]
})
export class ThankyouModule {
}
