import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserThankyouComponent} from './thankyou-view/thankyou.component';

const routes: Routes = [
  {
    path: 'complete',
    component: UserThankyouComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
