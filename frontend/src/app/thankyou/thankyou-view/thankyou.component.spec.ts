import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms'
import {UserThankyouComponent} from './thankyou/thankyou.component';

describe('UserThankyouComponent', () => {
  let component: UserThankyouComponent;
  let fixture: ComponentFixture<UserThankyouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserThankyouComponent],
      imports: [ReactiveFormsModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThankyouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
