import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute, Router} from "@angular/router";
import {map, Observable, Subscription} from "rxjs";
import {UserService} from 'src/app/core/_services/user.service';
import {OrdersService} from "../../core/_services/orders.service";


@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class UserThankyouComponent implements OnInit, OnDestroy {

  $orderId?: Observable<string | null>;

  private readonly subscriptions: Subscription = new Subscription();

  constructor(private userService: UserService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private orderService: OrdersService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.$orderId = this.activatedRoute.queryParams.pipe(map((queryParams) => queryParams['orderId']));
    this.subscriptions.add(
      this.$orderId.subscribe((orderId) => {
        if (orderId) {
          this.subscriptions.add(this.orderService.updateOrderStatusToPaid(orderId)
            .subscribe((success) => {
              if (success) {
                this.matSnackBar.open('Order successfully paid', 'OK', {duration: 3000});
              } else {
                this.matSnackBar.open('Order not paid', 'OK', {duration: 3000});
              }
            })
          );
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  retrievePurchase(order: any): void {
    const userId = this.userService.getCurrentUserId();
    this.orderService.updateOrderStatusToPaid(order.id).subscribe()

  }

  // only authenticated users can see this page
  // read endpoint to access this page:

  public goToHome() { // button sends user back home?
    this.router.navigate(['/']);
  }

}
