export interface AddItemToCartRequest {
  itemId?: string;
  orderId?: string;
  quantity: number;
  customerId?: string;
}
