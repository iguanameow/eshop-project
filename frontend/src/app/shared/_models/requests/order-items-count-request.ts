export interface OrderItemsCountRequest {
  orderId?: string;
  userId?: string;
}
