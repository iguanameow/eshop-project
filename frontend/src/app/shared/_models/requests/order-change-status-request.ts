export interface OrderChangeStatusRequest {
  orderId?: string;
  orderStatus: number;
}
