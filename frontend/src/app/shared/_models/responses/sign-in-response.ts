export interface SignInResponse {
  token: string;
  username: string;
  userId: string;
  isAdmin: boolean;
  success: boolean;
  message: string;
}
