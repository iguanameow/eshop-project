export interface AdminItemResponse {
  statusCode: number;
  message: string;
}
