export interface AddItemToCartResponse {
  requestStatus: boolean;
  message: string;
}
