export interface OrderItemsCountResponse {
  count: number;
  requestStatus: boolean;
}
