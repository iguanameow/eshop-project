export interface RemoveItemFromCartResponse {
  requestStatus: boolean;
  message: string;
}
