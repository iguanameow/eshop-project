export enum OrderStatus {
  CART,
  ORDERED,
  SHIPPED,
}
