export class UserDto {
  email: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  password: string | undefined;
  address: string | undefined;
  phone: string | undefined;
  isAdmin: boolean | undefined;
}
