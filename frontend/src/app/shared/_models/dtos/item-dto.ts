export interface ItemDto {
  id?: string;
  name: string;
  price: number;
  category: string;
  stock: number;
  description: string;
  image: string;
}
