import {OrderStatus} from "../enums/order-status";
import {OrderItemDto} from "./orderItem-dto";

export interface OrderDto {
  id: string;
  items: Array<OrderItemDto>;
  customerId: string;
  orderStatus: OrderStatus;
  orderStatusLabel: any;
  createdAt: any;
  customerFirstName: string;
  customerLastName: string;
  customerAddress: string;
  customerPhone: string;
  totalPrice: number;
}

export interface OrderDtoWithTotal extends OrderDto {
  total: number;
}
