import {ItemDto} from "./item-dto";

export interface OrderItemDto {
  item: ItemDto;
  quantity: number;
}
