import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CoreModule} from "../core/core.module";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    CoreModule,
  ],
  exports: []
})

/**
 * Components that are shared across the entire application should be declared here.
 */
export class SharedModule {
}
