import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

export class BaseHttpService {

  protected constructor(private basePath: string, public http: HttpClient) {
  }

  protected getBackendUrl(...endpoint: string[]): string {
    return `${environment.backendApi}/${this.basePath}/${endpoint.join('/')}`;
  }

  public get<T>(...endpoint: string[]): Observable<T> {
    return this.http.get<T>(this.getBackendUrl(...endpoint));
  }

  public post<T>(data: any, ...endpoint: string[]): Observable<T> {
    return this.http.post<T>(this.getBackendUrl(...endpoint), data);
  }

  public put<T>(data: any, ...endpoint: string[]): Observable<T> {
    return this.http.put<T>(this.getBackendUrl(...endpoint), data);
  }

  public delete<T>(...endpoint: string[]): Observable<T> {
    return this.http.delete<T>(this.getBackendUrl(...endpoint), {
      responseType: 'json',
    });
  }

}
