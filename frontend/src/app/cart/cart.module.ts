import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from '../core/core.module';

import {CartRoutingModule} from './cart-routing.module';
import {CartViewComponent} from './cart-view/cart-view.component';


@NgModule({
  declarations: [
    CartViewComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    CoreModule
  ]
})
export class CartModule {
}
