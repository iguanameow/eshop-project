import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {Subscription} from "rxjs";
import {OrdersService} from "../../core/_services/orders.service";
import {UserService} from "../../core/_services/user.service";
import {OrderDto} from "../../shared/_models/dtos/order-dto";

export interface CartItemList {
  name: string;
  quantity: number;
  price: number;
}

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.css']
})
export class CartViewComponent implements OnInit, OnDestroy {

  private readonly subscriptions: Subscription = new Subscription();
  orderId?: string;

  constructor(private userService: UserService,
              private orderService: OrdersService,
              private matSnackBar: MatSnackBar, private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.retrieveCartItems();
  }

  retrieveCartItems(): void {
    this.dataSource = [];
    const userId = this.userService.getCurrentUserId();
    if (!userId) {
      this.matSnackBar.open('You are not logged in', 'OK', {duration: 2000});
      return;
    }
    this.subscriptions.add(
      this.orderService.getCart(userId).subscribe(
        (order: OrderDto) => {
          if (!order) {
            this.matSnackBar.open('Your cart is empty', 'OK', {duration: 2000});
            return;
          }
          if (order.items.length > 0) {
            this.orderId = order.id;
          }
          this.dataSource = order.items.map((item: any) => {
            return {
              id: item.id,
              name: item.item.name,
              quantity: item.quantity,
              price: item.item.price
            };
          });
        }
      )
    );
  }

  removeItemFromCart(item: any) {
    this.orderService.removeItemFromCart(item.id).subscribe({
      next: () => {
        this.openSnackBar("Item removed successfully from cart.", "Dismiss");
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        setTimeout(function () {
          location.reload();
        }, 2000);
      }
    })

  }

  columnsToDisplay: string[] = ['name', 'quantity', 'price', 'action'];
  dataSource: CartItemList[] = []

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
}
