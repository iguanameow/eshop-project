import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from "../core/core.module";
import {HelloWorldRoutingModule} from './hello-world-routing.module';
import {HelloWorldComponent} from './hello-world/hello-world.component';


@NgModule({
  declarations: [
    HelloWorldComponent
  ],
  imports: [
    CommonModule,
    HelloWorldRoutingModule,
    CoreModule
  ]
})
export class HelloWorldModule {
}
