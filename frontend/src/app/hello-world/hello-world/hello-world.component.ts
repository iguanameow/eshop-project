import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {HelloDto} from "../../shared/_models/dtos/hello-dto";
import {HelloWorldService} from "../_services/hello-world.service";

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {
  $messages?: Observable<Array<HelloDto>>;

  constructor(private helloWorldService: HelloWorldService) {
  }

  ngOnInit(): void {
    this.$messages = this.helloWorldService.getHelloWorldObjects();
  }

}
