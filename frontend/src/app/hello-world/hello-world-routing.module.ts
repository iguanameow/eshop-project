import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HelloWorldComponent} from "./hello-world/hello-world.component";

const routes: Routes = [
  {
    path: '',
    component: HelloWorldComponent,
  },

  {
    path: '**',
    redirectTo: 'hello',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelloWorldRoutingModule {
}
