import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HelloDto} from "../../shared/_models/dtos/hello-dto";
import {BaseHttpService} from "../../shared/_services/base-http.service";

@Injectable({
  providedIn: 'root'
})
export class HelloWorldService extends BaseHttpService {

  constructor(public override http: HttpClient) {
    super('hello', http);
  }

  public getHelloWorldObjects(): Observable<Array<HelloDto>> {
    return this.get<Array<HelloDto>>('');
  }
}
