import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PaymentComponent} from './payment-view/payment.component';

const routes: Routes = [
  {
    path: 'provider',
    component: PaymentComponent,
  },
  {
    path: 'registration',
    redirectTo: '/registration',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule {
}
