import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from "../core/core.module";
import {PaymentRoutingModule} from './payment-routing.module';
import {PaymentComponent} from './payment-view/payment.component';

@NgModule({
  declarations: [
    PaymentComponent
  ],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    CoreModule
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentModule {
}
