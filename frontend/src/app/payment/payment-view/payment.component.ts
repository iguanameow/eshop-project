import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute, Router} from "@angular/router";
import {map, Observable} from "rxjs";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  form!: FormGroup;
  flag: boolean = true;
  $orderReference?: Observable<string | null>;

  constructor(private fb: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      card_name: [null, [Validators.required]],
      expiration_date: [null, [Validators.required]],
      card_number: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(16),
        Validators.maxLength(16)]],
      ccv: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(3)]]
    });
    this.$orderReference = this.activatedRoute.queryParams.pipe(map((queryParams) => queryParams['orderReference']));
  }

  public goToHome() {
    this.router.navigate(['/']);
  }

}
