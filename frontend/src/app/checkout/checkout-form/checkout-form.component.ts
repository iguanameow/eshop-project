import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {map, Observable, Subscription} from "rxjs";
import {OrdersService} from "../../core/_services/orders.service";
import {UserService} from "../../core/_services/user.service";
import {OrderDto, OrderDtoWithTotal} from "../../shared/_models/dtos/order-dto";
import {UserDto} from "../../shared/_models/dtos/user-dto";

@Component({
  selector: 'app-checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.css']
})
export class CheckoutFormComponent implements OnInit, OnDestroy {

  $orderId?: Observable<string>;
  $orderDto?: Observable<OrderDtoWithTotal>;

  form?: FormGroup;
  loading: boolean = false;

  private readonly subscriptions = new Subscription();

  constructor(private activatedRoute: ActivatedRoute,
              private orderService: OrdersService,
              private userService: UserService,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.$orderId = this.activatedRoute.params.pipe(map(params => {
      const orderId = params['id'];
      if (orderId) {
        this.$orderDto = this.orderService.getOrderById(orderId).pipe(map((orderDto: OrderDto) => {
          const newOrderDto = {...orderDto, total: 0};
          for (let item of orderDto.items) {
            newOrderDto.total += item.item.price * item.quantity;
          }
          this.loadUser(orderDto.customerId);
          return newOrderDto;
        }));
      }
      return orderId;
    }));

  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private buildForm(userDto: UserDto) {
    this.form = this.fb.group({
      firstName: [userDto.firstName, [Validators.required]],
      lastName: [userDto.lastName, [Validators.required]],
      address: [userDto.address, [Validators.required]],
      phone: [userDto.phone, [Validators.required]]
    });
  }

  private loadUser(userId: string): void {
    this.subscriptions.add(
      this.userService.getUserDto(userId).subscribe((userDto: UserDto) => {
        this.buildForm(userDto);
      })
    );
  }

  public onFormSubmit(form: FormGroup, orderDto: OrderDto): void {
    this.loading = true;

    orderDto.customerFirstName = form.controls['firstName'].value;
    orderDto.customerLastName = form.controls['lastName'].value;
    orderDto.customerAddress = form.controls['address'].value;
    orderDto.customerPhone = form.controls['phone'].value;

    this.subscriptions.add(
      this.orderService.updateOrder(orderDto.id, orderDto).subscribe((success: boolean) => {
        this.loading = false;
        this.router.navigate(['/payment', 'provider'], {queryParams: {orderReference: orderDto.id}})
      })
    );

  }

}
