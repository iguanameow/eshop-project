import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CheckoutFormComponent} from './checkout-form/checkout-form.component';

const routes: Routes = [
  {
    path: ':id',
    component: CheckoutFormComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule {
}
