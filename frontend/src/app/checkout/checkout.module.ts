import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from '../core/core.module';
import {CheckoutFormComponent} from './checkout-form/checkout-form.component';
import {CheckoutRoutingModule} from './checkout-routing.module';


@NgModule({
  declarations: [
    CheckoutFormComponent
  ],
  imports: [
    CommonModule,
    CheckoutRoutingModule,
    CoreModule
  ],
  exports: [
    CheckoutFormComponent
  ],
})
export class CheckoutModule {
}
