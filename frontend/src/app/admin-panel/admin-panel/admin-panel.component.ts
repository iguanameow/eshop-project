import {animate, state, style, transition, trigger,} from '@angular/animations';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminPanelComponent implements OnInit {
  emptyOrder = {
    itemsInOrder: [],
    customerId: '',
    orderStatus: 'CART',
    createdAt: '',
    customerFirstName: '',
    customerLastName: '',
    customerAddress: '',
    customerPhone: '',
  };
  orderForm: FormGroup;

  constructor(
    public fb2: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.reactiveOrderForm();
  }

  reactiveOrderForm() {
    this.orderForm = this.fb2.group({
      customerFirstName: [this.emptyOrder.customerFirstName],
      customerAddress: [this.emptyOrder.customerAddress],
      customerPhone: [this.emptyOrder.customerPhone],
      createdAt: [this.emptyOrder.createdAt],
      orderStatus: [this.emptyOrder.orderStatus],
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
}
