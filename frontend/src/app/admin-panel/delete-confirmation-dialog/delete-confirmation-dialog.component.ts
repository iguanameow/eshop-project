import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ItemsService} from 'src/app/core/_services/items.service';

@Component({
  selector: 'app-delete-confirmation-dialog',
  templateUrl: './delete-confirmation-dialog.component.html',
  styleUrls: ['./delete-confirmation-dialog.component.css']
})
export class DeleteConfirmationDialogComponent implements OnInit {

  id: string;

  constructor(
    private deleteConfirmationService: ItemsService,
    public dialogRef: MatDialogRef<DeleteConfirmationDialogComponent>,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    this.id = this.data;
  }

  deleteItem() {
    this.deleteConfirmationService.deleteItem(this.id).subscribe({
      next: (response) => {
        this.openSnackBar(response.message, 'Dismiss');
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        setTimeout(function () {
          location.reload();
        }, 1000);
      },
    })
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

}
