import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../shared/_services/base-http.service';

@Injectable({
  providedIn: 'root',
})
export class AdminPanelService extends BaseHttpService {

  constructor(public override http: HttpClient) {
    super('admin-panel', http);
  }
}
