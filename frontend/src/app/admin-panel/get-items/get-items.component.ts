import {animate, state, style, transition, trigger,} from '@angular/animations';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ItemsService} from '../../core/_services/items.service';
import {ItemDto} from '../../shared/_models/dtos/item-dto';

import {DeleteConfirmationDialogComponent} from '../delete-confirmation-dialog/delete-confirmation-dialog.component';

@Component({
  selector: 'app-get-items',
  templateUrl: './get-items.component.html',
  styleUrls: ['./get-items.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class GetItemsComponent implements OnInit {
  items: any;

  constructor(
    private adminPanelService: ItemsService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.retrieveItems();
  }

  retrieveItems(): void {
    this.adminPanelService.getItems().subscribe({
      next: (data) => {
        this.items = data;
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.items);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  openDeleteConfirmationDialog(item: any) {
    this.dialog.open(DeleteConfirmationDialogComponent, {
      data: item.id,
    });
  }

  columnsToDisplay: string[] = [
    'name',
    'price',
    'category',
    'stock',
    'action',
  ];

  expandedElement: ItemDto | null;
  dataSource: MatTableDataSource<ItemDto>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
