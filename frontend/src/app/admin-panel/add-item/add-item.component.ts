import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ItemsService} from '../../core/_services/items.service';
import {ItemDto} from '../../shared/_models/dtos/item-dto';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css'],
})
export class AddItemComponent implements OnInit {
  emptyItem = {
    name: '',
    price: 0,
    category: '',
    stock: 0,
    description: '',
    image: '',
  };

  itemForm: FormGroup;
  validationError = false;
  isSubmitButtonDisabled = false;

  constructor(
    private adminPanelService: ItemsService,
    public fb: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.reactiveItemForm();
  }

  reactiveItemForm() {
    this.itemForm = this.fb.group({
      itemName: [this.emptyItem.name, [Validators.required]],
      itemPrice: [this.emptyItem.price, [Validators.required]],
      itemCategory: [this.emptyItem.category, [Validators.required]],
      itemStock: [this.emptyItem.stock, [Validators.required]],
      itemDescription: this.emptyItem.description,
      itemImage: [this.emptyItem.image, [Validators.required]],
    });
  }

  saveItem(): void {
    const item: ItemDto = {
      name: this.itemForm.value.itemName,
      price: this.itemForm.value.itemPrice,
      category: this.itemForm.value.itemCategory,
      stock: this.itemForm.value.itemStock,
      description: this.itemForm.value.itemDescription,
      image: this.itemForm.value.itemImage,
    };

    if (this.inputsAreValid(item)) {
      this.validationError = true;
    } else {
      this.validationError = false;
      this.adminPanelService.createItem(item).subscribe({
        next: (response) => {
          this.openSnackBar(response.message, 'Dismiss');
        },
        error: (error) => {
          console.log(error);
        },
        complete: () => {
          this.isSubmitButtonDisabled = true;
          setTimeout(function () {
            location.reload();
          }, 1000);
        },
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  inputsAreValid(item: any) {
    return (
      item.name.valueOf().trim() == '' ||
      item.category.valueOf().trim() == '' ||
      item.image.valueOf().trim() == '' ||
      item.price < 0 ||
      item.stock < 0
    );
  }
}
