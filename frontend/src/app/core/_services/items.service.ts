import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {AdminItemResponse} from "src/app/shared/_models/responses/admin-item-response";
import {ItemDto} from "../../shared/_models/dtos/item-dto";
import {BaseHttpService} from "../../shared/_services/base-http.service";

@Injectable({
  providedIn: 'root'
})
export class ItemsService extends BaseHttpService {

  constructor(public override http: HttpClient) {
    super('api/items', http);
  }


  getItems(): Observable<Array<ItemDto>> {
    return this.get();
  }

  getItem(id: string): Observable<ItemDto> {
    return this.get(`${id}`);
  }

  createItem(item: ItemDto): Observable<AdminItemResponse> {
    return this.post<AdminItemResponse>(item, '');
  }

  updateItem(id: string, value: ItemDto): Observable<AdminItemResponse> {
    return this.put<AdminItemResponse>(value, `${id}`);
  }

  deleteItem(id: string): Observable<AdminItemResponse> {
    return this.delete<AdminItemResponse>(`${id}`);
  }
}
