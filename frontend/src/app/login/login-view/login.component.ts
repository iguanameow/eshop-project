import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {UserService} from 'src/app/core/_services/user.service';
import {AuthService} from "../../core/_services/auth.service";
import {SignInRequest} from "../../shared/_models/requests/sign-in-request";
import {SignInResponse} from "../../shared/_models/responses/sign-in-response";

/**
 TODO: Fix rerouting back to homepage and fix hashing of password
 */

@Component({
  selector: 'app-user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class UserLoginComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  flag: boolean = true;

  private readonly subscriptions: Subscription = new Subscription();

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private router: Router,
              private authService: AuthService,
              private snackBar: MatSnackBar) {
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      firstName: [null],
      password: [null, [Validators.required, Validators.minLength(6)]],
    });
  }

  registration(form: FormGroup): void {
    const request: SignInRequest = {
      username: form.controls['email'].value,
      password: form.controls['password'].value
    }

    this.subscriptions.add(
      this.authService.signIn(request).subscribe(
        (response: SignInResponse) => {
          if (response.success) {
            this.authService.setLocalStorage(response);
            this.goToHome();
            this.snackBar.open('You have successfully logged in!');
          } else {
            this.snackBar.open(response.message);
          }
        }
      )
    );
  }

  public goToHome() {
    this.router.navigate(['/']);
  }

}
