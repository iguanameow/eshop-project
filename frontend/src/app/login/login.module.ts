import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CoreModule} from "../core/core.module";
import {UserRoutingModule} from './login-routing.module';
import {UserLoginComponent} from './login-view/login.component';

@NgModule({
  declarations: [
    UserLoginComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    CoreModule
  ],
  exports: [
    UserLoginComponent
  ]
})
export class LoginModule {
}
