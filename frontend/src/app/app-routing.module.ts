import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from './shared/_guards/admin.guard';
import {AuthGuard} from './shared/_guards/auth.guard';

const routes: Routes = [
  {
    path: 'hello',
    redirectTo: '/'
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./catalogue/catalogue.module').then((m) => m.CatalogueModule)
  },
  {
    path: 'admin-panel',
    canActivate: [AuthGuard, AdminGuard],
    loadChildren: () =>
      import('./admin-panel/admin-panel.module').then(
        (m) => m.AdminPanelModule
      ),
  },
  {
    path: 'registration',
    loadChildren: () =>
      import('./user/user.module').then(
        (m) => m.UserModule
      ),
  },
  {
    path: 'thank-you',
    loadChildren: () => import('./thankyou/thankyou.module').then((m) => m.ThankyouModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule)
  },
  {
    path: 'cart',
    canActivate: [AuthGuard],
    loadChildren: () => import('./cart/cart.module').then((m) => m.CartModule),
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then((m) => m.PaymentModule)
  },
  {
    path: 'checkout',
    canActivate: [AuthGuard],
    loadChildren: () => import('./checkout/checkout.module').then((m) => m.CheckoutModule),

  },
  {
    path: '',
    redirectTo: '/catalogue',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
